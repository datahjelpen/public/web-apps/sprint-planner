import { Store, type StoreChildConstructorOptions } from '@datahjelpen/svale-ui/classes/Store';

export interface Settings {
	effectiveWorkDay;
	slack;
	sprintLength;
	sprintLengthDeductables: {
		planning;
		refinementDailyMeeting;
		retroReview;
	};
	actualSprintLength;
}

export class SettingsStore extends Store {
	constructor(options: StoreChildConstructorOptions) {
		super({
			defaultStateValue: options.defaultState,
			storeName: 'settings'
		});
	}

	public save = (data) => {
		this.set(data);
	};

	public resetToInitial = (): void => {
		this.reset();
	};
}
