import { Store, type StoreChildConstructorOptions } from '@datahjelpen/svale-ui/classes/Store';

export interface Row {
	name: string;
	efforts: {
		percentage: number;
	};
	notAvailable: number;
	category: string;
}

export class RowsStore extends Store {
	constructor(options: StoreChildConstructorOptions) {
		super({
			defaultStateValue: options.defaultState,
			storeName: 'rows'
		});
	}

	public save = (data) => {
		this.set(data);
	};

	public resetToInitial = (): void => {
		this.reset();
	};
}
